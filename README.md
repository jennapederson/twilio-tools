# Twilio Tools

## Setup

Copy `.env.example` to `.env` and update the variables for your environment and Twilio account.

## Triggering a Studio Flow

Triggers an existing studio flow.

`ruby trigger_followup_studio_flow.rb`

## Notifying a list of phone numbers via SMS

`ruby notify_sms.rb`