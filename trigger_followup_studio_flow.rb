require 'dotenv/load'
require 'twilio-ruby'

ACCOUNT_SID = ENV['TWILIO_ACCOUNT_SID']
AUTH_TOKEN = ENV['TWILIO_AUTH_TOKEN']
FROM_PHONE_NUMBER = ENV['FROM_PHONE_NUMBER']
TWILIO_STUDIO_FLOW = ENV['TWILIO_STUDIO_FLOW']
TO_PHONE_NUMBERS = ENV['TO_PHONE_NUMBERS']

to_phone_numbers = TO_PHONE_NUMBERS.split(/\s+/)

puts "\n\nReady to send to #{to_phone_numbers.uniq}? Type 'YES' to continue. Anything else exits.\n\n"
ready = gets.chomp

if ready == "YES"
  # puts "to_phone_numbers: #{to_phone_numbers.uniq}"

  client = Twilio::REST::Client.new(ACCOUNT_SID, AUTH_TOKEN).studio.flows(TWILIO_STUDIO_FLOW)
  to_phone_numbers.uniq.each do |phone_number|
    begin
      puts "triggering followup studio flow for phone_number: #{phone_number}"
      execution = client.executions.create(from: FROM_PHONE_NUMBER, to: phone_number, parameters: '{"rest_flow":"followup"}')
      puts execution.inspect
    rescue Twilio::REST::TwilioError => error
      puts error.message
    end
  end
else
  puts "Exiting without triggering followup studio flow."
end