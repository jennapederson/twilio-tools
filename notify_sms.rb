require 'dotenv/load'
require 'twilio-ruby'

account_sid = ENV['TWILIO_ACCOUNT_SID']
auth_token = ENV['TWILIO_AUTH_TOKEN']
notify_sid = ENV['TWILIO_NOTIFY_SERVICE_SID']

client = Twilio::REST::Client.new(account_sid, auth_token)

service = client.notify.v1.services(notify_sid)

# binding = service.bindings.create(
#   identity: '00000001',
#   binding_type: 'sms',
#   address: '+15555551234'
# )
# puts binding.sid

notification = service.notifications.create(
  # identity: '00000001',
  to_binding: [
    '{"binding_type":"sms", "address":"+15555551235"}',
    '{"binding_type":"sms", "address":"+15555551234"}'
  ],
  body: 'Hello! Hope you\'re having a good day!'
)
puts notification.sid

